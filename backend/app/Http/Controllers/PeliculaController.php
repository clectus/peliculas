<?php

namespace App\Http\Controllers;

use App\Models\Pelicula;
use Illuminate\Http\Request;

class PeliculaController extends Controller
{
   
    public function index()
    {
        return Pelicula::all();
    }

    
    public function store(Request $request)
    {

        $request->validate([
            'titulo' => ['required'],
            'tiempo' => ['required']
        ]);

        $pelicula = new Pelicula;
        $pelicula->titulo = $request->titulo;
        $pelicula->tiempo = $request->tiempo;
        $pelicula->save();
        return $pelicula;
        
    }

    
    public function show(Pelicula $pelicula)
    {
        return $pelicula;
    }

    
    public function update(Request $request, Pelicula $pelicula)
    {
        $request->validate([
            'titulo' => ['required'],
            'tiempo' => ['required']
        ]);

        $pelicula->titulo = $request->titulo;
        $pelicula->tiempo = $request->tiempo;
        $pelicula->save();
        return $pelicula;
    }

    
    public function destroy(Pelicula $pelicula)
    {
        $pelicula->delete();
        return response()->noContent();
    }
}
