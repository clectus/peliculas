<?php

namespace Tests\Feature;

use App\Models\Pelicula;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class PeliApiTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    function getAllPelis()
    {
        $peliculas = Pelicula::factory(7)->create();

        $this->getJson(route('peliculas.index'))
            ->assertJsonFragment([
                'titulo' => $peliculas[0]->titulo,
                'tiempo' => $peliculas[0]->tiempo
            ])->assertJsonFragment([
                'titulo' => $peliculas[1]->titulo,
                'tiempo' => $peliculas[1]->tiempo
            ]);
    }

    /** @test */
    function getOnePeli()
    {
        $pelicula = Pelicula::factory()->create();

        $this->getJson(route('peliculas.show', $pelicula))
            ->assertJsonFragment([
                'titulo' => $pelicula->titulo,
                'tiempo' => $pelicula->tiempo,
            ]);
    }

    /** @test */
    function getCreatePelis()
    {
        
        $this->postJson(route('peliculas.store'), [])
            ->assertJsonValidationErrorFor('titulo');

        $this->postJson(route('peliculas.store'), [])
            ->assertJsonValidationErrorFor('tiempo');

        $this->postJson(route('peliculas.store'), [
            'titulo' => 'Nueva pelicula',
            'tiempo' => '1789'
        ])->assertJsonFragment([
            'titulo' => 'Nueva pelicula',
            'tiempo' => '1789'    
        ]);

        $this->assertDatabaseHas('peliculas', [
            'titulo' => 'Nueva pelicula',
            'tiempo' => '1789'
        ]);
    }

    /** @test */
    function getUpdatePeli()
    {
        $pelicula = Pelicula::factory()->create();

        $this->patchJson(route('peliculas.update', $pelicula), [])
            ->assertJsonValidationErrorFor('titulo');
        $this->patchJson(route('peliculas.update', $pelicula), [])
            ->assertJsonValidationErrorFor('tiempo');
        
        $this->patchJson(route('peliculas.update', $pelicula), [
            'titulo' => 'Nueva pelicula editada',
            'tiempo' => '1900'
        ])->assertJsonFragment([
            'titulo' => 'Nueva pelicula editada',
            'tiempo' => '1900'    
        ]);

        $this->assertDatabaseHas('peliculas', [
            'titulo' => 'Nueva pelicula editada',
            'tiempo' => '1900'
        ]);
    }

    /** @test */
    function getDeletePeli()
    {
        $pelicula = Pelicula::factory()->create();

        $this->deleteJson(route('peliculas.destroy', $pelicula))
            ->assertNoContent();
        
        $this->assertDatabaseCount('peliculas', 0);
    }
}
